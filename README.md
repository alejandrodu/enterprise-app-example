enterprise-app-example
======================

This is the official enterprise-app Vaadin add-on example/reference application.

Instructions
============

 - Import the project in Eclipse (or your favorite IDE).
 - Add enterprise-app jar and its dependencies to your classpath (usually in WEB-INF/lib).
 - Compile widgetset.
 - Have fun!

You can download the required jars from Vaadin Directory (https://vaadin.com/directory#addon/enterprise-app).
